import { useState } from 'react'

function App() {
  return (
    <div className="grid h-screen place-items-center">
      <div className="max-w-2xl p-5">
        <div className="grid place-items-center rounded-3xl shadow-2xl sm:grid-cols-2">
          <div className="grid w-72 place-items-center gap-5 rounded-t-none rounded-b-3xl bg-gradient-to-tr  from-bg-light-slate-blue to-bg-light-royal-blue p-5 text-neutral-white/70 sm:rounded-3xl">
            <h1 className="text-lg font-semibold">Your Result</h1>
            <div className="grid aspect-square w-40 place-items-center rounded-full bg-gradient-to-b from-bg-violet-blue/80 to-bg-persian-blue/5">
              <p className="flex flex-col place-items-center">
                <span className="text-6xl font-semibold text-neutral-white">
                  76
                </span>
                <span className="text-sm text-neutral-white/40">of 100</span>
              </p>
            </div>
            <div className="grid gap-3 text-center">
              <h2 className="text-xl font-semibold text-neutral-white">
                Great
              </h2>
              <p className="w-44 text-sm">
                You scored higher than 65% of the people who have taken these
                tests.
              </p>
            </div>
          </div>
          <div className="grid w-72 gap-5 px-7 py-5">
            <h1 className="text-lg font-semibold">Summary</h1>
            <div className="grid grid-flow-row gap-2">
              <div className="grid h-12 grid-flow-col items-center justify-between rounded-lg bg-primary-light-red/10 px-2 text-sm font-semibold text-primary-light-red">
                <span className="grid grid-flow-col gap-2">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="none"
                    viewBox="0 0 20 20"
                    className="stroke-primary-light-red"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M10.833 8.333V2.5l-6.666 9.167h5V17.5l6.666-9.167h-5Z"
                    />
                  </svg>
                  <span>Reaction</span>
                </span>
                <span className="text-neutral-dark-gray-blue">
                  <span className="font-bold">80</span>{' '}
                  <span className="text-neutral-dark-gray-blue/50">/ 100</span>
                </span>
              </div>
              <div className="grid h-12 grid-flow-col items-center justify-between rounded-lg bg-primary-orangey-yellow/10 px-2 text-sm font-semibold text-primary-orangey-yellow">
                <span className="grid grid-flow-col gap-2">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="none"
                    viewBox="0 0 20 20"
                    className="stroke-primary-orangey-yellow"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M5.833 11.667a2.5 2.5 0 1 0 .834 4.858"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M3.553 13.004a3.333 3.333 0 0 1-.728-5.53m.025-.067a2.083 2.083 0 0 1 2.983-2.824m.199.054A2.083 2.083 0 1 1 10 3.75v12.917a1.667 1.667 0 0 1-3.333 0M10 5.833a2.5 2.5 0 0 0 2.5 2.5m1.667 3.334a2.5 2.5 0 1 1-.834 4.858"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M16.447 13.004a3.334 3.334 0 0 0 .728-5.53m-.025-.067a2.083 2.083 0 0 0-2.983-2.824M10 3.75a2.085 2.085 0 0 1 2.538-2.033 2.084 2.084 0 0 1 1.43 2.92m-.635 12.03a1.667 1.667 0 0 1-3.333 0"
                    />
                  </svg>
                  <span>Memory</span>
                </span>
                <span className="text-neutral-dark-gray-blue">
                  <span className="font-bold">92</span>{' '}
                  <span className="text-neutral-dark-gray-blue/50">/ 100</span>
                </span>
              </div>
              <div className="grid h-12 grid-flow-col items-center justify-between rounded-lg bg-primary-green-teal/10 px-2 text-sm font-semibold text-primary-green-teal">
                <span className="grid grid-flow-col gap-2">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="none"
                    viewBox="0 0 20 20"
                    className="stroke-primary-green-teal"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M7.5 10h5M10 18.333A8.333 8.333 0 1 0 1.667 10c0 1.518.406 2.942 1.115 4.167l-.699 3.75 3.75-.699A8.295 8.295 0 0 0 10 18.333Z"
                    />
                  </svg>
                  <span>Verbal</span>
                </span>
                <span className="text-neutral-dark-gray-blue">
                  <span className="font-bold">61</span>{' '}
                  <span className="text-neutral-dark-gray-blue/50">/ 100</span>
                </span>
              </div>
              <div className="grid h-12 grid-flow-col items-center justify-between roundedLg bg-primary-cobalt-blue/10 px-2 text-sm font-semibold text-primary-cobalt-blue">
                <span className="grid grid-flow-col gap-2">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="20"
                    height="20"
                    fill="none"
                    viewBox="0 0 20 20"
                    className="stroke-primary-cobalt-blue"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M10 11.667a1.667 1.667 0 1 0 0-3.334 1.667 1.667 0 0 0 0 3.334Z"
                    />
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.25"
                      d="M17.5 10c-1.574 2.492-4.402 5-7.5 5s-5.926-2.508-7.5-5C4.416 7.632 6.66 5 10 5s5.584 2.632 7.5 5Z"
                    />
                  </svg>
                  <span>Visual</span>
                </span>
                <span className="text-neutral-dark-gray-blue">
                  <span className="font-bold">72</span>{' '}
                  <span className="text-neutral-dark-gray-blue/50">/ 100</span>
                </span>
              </div>
            </div>
            <button className="h-10 rounded-full bg-neutral-dark-gray-blue text-sm font-semibold text-neutral-white hover:bg-gradient-to-b hover:from-bg-light-slate-blue hover:to-bg-light-royal-blue">
              Continue
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
