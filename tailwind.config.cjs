/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Hanken Grotesk"', ...defaultTheme.fontFamily.sans]
      },
      colors: {
        'primary-light-red': 'hsl(0, 100%, 67%)',
        'primary-orangey-yellow': 'hsl(39, 100%, 56%)',
        'primary-green-teal': 'hsl(166, 100%, 37%)',
        'primary-cobalt-blue': 'hsl(234, 85%, 45%)',
        'bg-light-slate-blue': 'hsl(252, 100%, 67%)',
        'bg-light-royal-blue': 'hsl(241, 81%, 54%)',
        'bg-violet-blue': 'hsl(256, 72%, 46%)',
        'bg-persian-blue': 'hsl(241, 72%, 46%)',
        'neutral-white': 'hsl(0, 0%, 100%)',
        'neutral-pale-blue': 'hsl(221, 100%, 96%)',
        'neutral-light-lavender': 'hsl(241, 100%, 89%)',
        'neutral-dark-gray-blue': 'hsl(224, 30%, 27%)'
      }
    }
  },
  plugins: []
}
